// let x = 10
// if (i == 1) {
//     let x = 11;
//     console.log(x)
// }
// console.log(x)

// const x = "Hello";
// x = "World"

// var x;
// let x;
// const x = 5;
// console.log(x);

// let x = 10;
// var x = 5;

// console.log(x)

//Array
// var a = [5, //Number
//     "Hello", //String
//     5.2, //Floating Number
//     false, //Boolean
//     function sum() { //Function
//         return 5 + 10;
//     }
// ];

// const func = a[4];
// console.log(func())

//Object(Associate Array (PHP))
// { "key": "value",}

// var info = {
//     name: "K",
//     age: 21,
// }
// const name = info.name
// const age = info.age

// console.log(name)
// console.log(age)

//Object Destructuring
// var info = {
//     name: "K",
//     age: 21,
// }

// var { name, age } = info
// console.log(name, age)

//2
// var text = 'Hello'
// var [a, b, c, d, e] = text
// console.log(a)

//No return (void)
// function SayHello(){
//     alert('Hello')
// }
//SayHello()

//return value
// function Sum(){
//     return 15
// }
// const result = Sum()
// console.log(result)

//function return function
// function getA(){
//     return function getB(){
//         return "Get B"
//     }
// }
// const result=getA()
// console.log(result())