import React from 'react';
import './App.css';
import Input from './component/Input'
import List from './component/List'
import Todo from './component/Todo'
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends React.Component {
  state = {
    key: 1,
    lists: []
  }

  addList = (name) => {
    let lists = this.state.lists;
    lists.push(new Todo(this.state.key, name));
    this.setState({
      key: this.state.key + 1,
      lists: lists
    });
  }

  render() {
    return (
      <div className="App row justify-content-center">
        <div>
          <Input addList={this.addList} />
          <List lists={this.state.lists} />
        </div>
        <div className="w-100"></div>
      </div>

    );
  }
}

export default App;
