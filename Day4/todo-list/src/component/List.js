import React from 'react';

class List extends React.Component {

    tableData = () => {
        return this.props.lists.map((list) => {
            return (
                <tr key={list.key}>
                    <td>{list.key}</td>
                    <td>{list.name}</td>
                    <td><button type="button" className="btn btn-secondary">Done</button>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Todo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.tableData()}
                </tbody>
            </table>
        )
    }
}

export default List