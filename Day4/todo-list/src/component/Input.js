import React from 'react';

class Input extends React.Component {

    addList = () => {
        let name = document.getElementById("name").value;
        this.props.addList(name);
      }

    render() {
        return (
            <form>
                <div className="form-row">
                    <div className="col">
                        <input type="text" id="name" className="form-control" placeholder="Name" />
                    </div>
                    <button type="button" className="btn btn-primary" onClick={this.addList}>ADD</button>
                </div>
            </form>
        )
    }
}

export default Input