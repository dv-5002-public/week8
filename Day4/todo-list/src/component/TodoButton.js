import React from 'react';

const TodoButton = {
    TODO: 'todo',
    DONE: 'done',
    REMOVE: 'remove'
}

export default TodoButton