import React from 'react';
import './App.css';
import Header from './component/Header'
import Body from './component/BodyClass'
import Footer from './component/Footer'

// function App() {
//   return (
//     <h1>Hello World</h1>
//   );
// }

// const App = function () {
//   return (
//     <h1>Hello World</h1>
//   );
// }

const App = () => {
  return (
    <div>
      <Header 
      title="Hello CAMT"
      />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
