import React from 'react';
import Clock from './Clock'

class BodyClass extends React.Component {

    // constructor(props){
    //     super(props)

    //     this.state={
    //         title="Current Time"
    //     }
    //     this.logTime=this.logTime.bind(this)
    // }

    // longTime(time) {
    //     console.log(time.toTimeString())
    // }

    state = {
        title: "Current Time"
    }

    longTime = (time) => {
        console.log(time.toTimeString())
    }

    formatChanged = (useUTC) =>{
        console.log(`useUTC = ${useUTC}`)
    }

    render() {
        return (
            <div>
                <h4>{this.state.title}</h4>
                <Clock
                    onTimeChanged={this.logTime}
                    onFormatChanged={this.formatChanged} />
            </div>

        )
    }
}




export default BodyClass