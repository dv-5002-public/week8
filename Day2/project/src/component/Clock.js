import React from 'react';

class Clock extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            date: new Date(),
            useUTC: true
        }

        // this.changeFormat = this.changeFormat.bind(this);
    }

    componentDidMount() {
        this.timer = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    tick() {
        this.setState({
            date: new Date()
        })
    }

    // changeFormat() {
    //     this.setState({
    //         useUTC: !this.state.useUTC
    //     })
    // }

    changeFormat = () => {
        this.setState({
            useUTC: !this.state.useUTC
        })
    }

    render() {
        return (
            // <h2>{this.state.useUTC ?
            //     this.state.date.toUTCString() //If this.state.useUTC == true
            //     :
            //     this.state.date.toTimeString()
            // }</h2>

            <div>
                {
                    this.state.useUTC ?
                        <h2>{this.state.date.toUTCString()}</h2>
                        :
                        <h2>{this.state.date.toTimeString()}</h2>
                }
                <button onClick={this.changeFormat}>ChangeFormat</button>
            </div>
        )
    }
}

export default Clock