import React from 'react';
import { render } from '@testing-library/react';

const Button = (props) =>{
        return (
            <tbody>
                <tr>
                    <td><input type="button" className="btn btn-secondary" value="1" onClick={() => props.onClickButton(1)} /> </td>
                    <td><input type="button" className="btn btn-secondary" value="2" onClick={() => props.onClickButton(2)} /> </td>
                    <td><input type="button" className="btn btn-secondary" value="3" onClick={() => props.onClickButton(3)} /> </td>
                    <td><input type="button" className="btn btn-warning" value="+" onClick={() => props.onClickButton('+')} /> </td>
                </tr>
                <tr>
                    <td><input type="button" className="btn btn-secondary" value="4" onClick={() => props.onClickButton(4)} /> </td>
                    <td><input type="button" className="btn btn-secondary" value="5" onClick={() => props.onClickButton(5)} /> </td>
                    <td><input type="button" className="btn btn-secondary" value="6" onClick={() => props.onClickButton(6)} /> </td>
                    <td><input type="button" className="btn btn-warning" value="-" onClick={() => props.onClickButton('-')} /> </td>
                </tr>
                <tr>
                    <td><input type="button" className="btn btn-secondary" value="7" onClick={() => props.onClickButton(7)} /> </td>
                    <td><input type="button" className="btn btn-secondary" value="8" onClick={() => props.onClickButton(8)} /> </td>
                    <td><input type="button" className="btn btn-secondary" value="9" onClick={() => props.onClickButton(9)} /> </td>
                    <td><input type="button" className="btn btn-warning" value="*" onClick={() => props.onClickButton('*')} /> </td>
                </tr>
                <tr>
                    <td><input type="button" className="btn btn-secondary" value="0" onClick={() => props.onClickButton(0)} /> </td>
                    <td colSpan="2"><input type="button" className="btn btn-info" value="=" onClick={() => props.onClickButton('=')} /> </td>
                    <td><input type="button" className="btn btn-warning" value="/" onClick={() => props.onClickButton('/')} /> </td>
                </tr>
                <tr>
                    <td colSpan="6"><input type="button" className="btn btn-warning" value="Clear" onClick={() => props.onClickButton('C')} /> </td>
                </tr>
            </tbody>
        )
    }

export default Button