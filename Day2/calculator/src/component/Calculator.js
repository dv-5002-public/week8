import React from 'react';
import Display from './Display'
import Button from './Button'

class Calculator extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            input: ""
        }
    }


    addInput = value => {
        let element = document.getElementById("result").value
        let re = element.substr(-1);
        if (isNaN(re) && (value === '+' || value === '-' || value === '*' || value === '/')) {
        } else {
            if (value === '=') {
                this.setState({
                    input: this.state.input + "=" + eval(this.state.input)
                })
            } else if (value === 'C') {
                this.setState({
                    input: this.state.input = ""
                })
            } else {
                this.setState({
                    input: this.state.input += value
                })
            }
        }
    }

    render() {
        return (
            <table>
                <tbody>
                    <tr>
                        <Display display={this.state.input} />
                    </tr>
                </tbody>
                <Button onClickButton={this.addInput} />
            </table>
        )
    }
}

export default Calculator