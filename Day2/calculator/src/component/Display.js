import React from 'react';

const Display = (props) => {
    return (
        <td colSpan="6"><input type="text" className="form-control" id="result" value={props.display} readOnly /></td>
    )
}

export default Display