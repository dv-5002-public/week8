import React from 'react';
import './App.css';
import Calculator from './component/Calculator'
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  return (
    <div className="d-flex justify-content-center">
      <Calculator />
    </div>
  );
}

export default App;
