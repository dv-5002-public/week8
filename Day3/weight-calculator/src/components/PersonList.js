import React from 'react';

class PersonList extends React.Component {

  renderTableData = () => {
    return this.props.persons.map((person) => {
      return (
        <tr key={person.key}>
          <td>{person.key}</td>
          <td>{person.name}</td>
          <td>{person.weight}</td>
        </tr>
      );
    })
  }
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Weight</th>
          </tr>
        </thead>
        <tbody>
          {this.renderTableData()}
        </tbody>
      </table>
    );
  }
}

export default PersonList;