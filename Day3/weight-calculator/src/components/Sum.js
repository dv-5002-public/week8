import React from 'react';

class Summary extends React.Component {

  getAverage = () => {
    let sum = 0;
    this.props.persons.forEach(element => {
      sum += parseInt(element.weight);
    });
    return sum / this.props.persons.length
  }
  
  render() {
    return (
      <div>
        <h5>
          The average is: {isNaN(this.getAverage()) ? '' : this.getAverage()}
        </h5>
      </div>
    );
  }
}

export default Summary;