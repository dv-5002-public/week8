import React from 'react';

class Input extends React.Component {

  addPerson = () => {
    let name = document.getElementById("name").value;
    let weight = document.getElementById("weight").value
    this.props.addPerson(name, weight);
  }
  
  render() {
    return (
      <form>
        <div className="form-row">
          <div className="col">
            <input type="text" id="name" className="form-control" placeholder="Name" />
          </div>
          <div className="col">
            <input type="text" id="weight" className="form-control" placeholder="Weight" />
          </div>
          <button type="button" className="btn btn-dark" onClick={this.addPerson}>Add</button>
        </div>
      </form>
    );
  }
}

export default Input;