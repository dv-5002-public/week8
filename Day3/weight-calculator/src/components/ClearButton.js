import React from 'react';

const ClearButton = (props) => {
  const clear = () => {
    props.clearData();
  }

  return (
    <div style={{textAlign: "center"}}>
      <button className="btn btn-dark" onClick={clear}>Clear</button>
    </div>
  );
}

export default ClearButton;