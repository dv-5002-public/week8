import React from 'react';
import './App.css';
import Input from './components/Input';
import Person from './components/Person';
import PersonList from './components/PersonList';
import Summary from './components/Sum';
import ClearButton from './components/ClearButton';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends React.Component {
  state = {
    key: 1,
    persons: []
  }

  addPerson = (name, weight) => {
    let persons = this.state.persons;
    persons.push(new Person(this.state.key, name, weight));
    this.setState({
      key: this.state.key + 1,
      persons: persons
    });
  }

  clearData = () => {
    this.setState({
      persons: []
    });
  }

  render() {
    return (
        <div className="App row justify-content-center">
          <div>
            <Input addPerson={this.addPerson} />
            <PersonList persons={this.state.persons} />
          </div>
          <div className="w-100"></div>
          <div>
            <Summary persons={this.state.persons} />
          </div>
          <div className="w-100"></div>
          <div>
            <ClearButton clearData={this.clearData} />
          </div>
        </div>
    );
  }
}

export default App;
